#!/usr/bin/env bash
DIR="$(dirname ${0})/../../"

cd ${DIR}

if [[ ! -f ".env" ]]; then
    printf "ERROR: \".env\" does not exists \n"
    exit 1
fi

ENV_KEYS=$(mktemp)
ENV_EXAMPLE_KEYS=$(mktemp)

cat .env | grep "=" | grep -v "#" | cut -d= -f1 | sort > ${ENV_KEYS}
cat .env.example | grep "=" | grep -v "#" | cut -d= -f1 | sort > ${ENV_EXAMPLE_KEYS}

DIFF=$(mktemp)
diff -u ${ENV_KEYS} ${ENV_EXAMPLE_KEYS} > ${DIFF}
DIFF_FILTERED=$(mktemp)
cat ${DIFF} | grep -v "/tmp" | grep -v "@" | grep "+" > ${DIFF_FILTERED}

if [[ $(cat ${DIFF_FILTERED} | wc -l) -gt 0 ]]; then
    MISSING_PARAMS=$(cat ${DIFF_FILTERED} | paste -sd " " | sed "s|+||g")
    printf "ERROR: some parameters are missing in .env file: ${MISSING_PARAMS}\n"
    EXIT_CODE=1
else
    EXIT_CODE=0
fi

rm ${ENV_KEYS}
rm ${ENV_EXAMPLE_KEYS}
rm ${DIFF}
rm ${DIFF_FILTERED}

if [[ ${EXIT_CODE} -ne 0 ]]; then
    exit 1
fi

TIMEZONE=$(grep -r TIMEZONE .env | cut -d= -f2)
ls -l "/usr/share/zoneinfo/${TIMEZONE}" > /dev/null
if [[ $? -ne 0 ]]; then
    printf "ERROR: the timezone you specified could not be found\n"
    exit 1
fi

USER_ID=$(grep -r UID .env | cut -d= -f2)
if [[ ${USER_ID} -eq 0 ]]; then
    printf "ERROR: the user id must not be 0 (root user id)\n"
    exit 1
else
    exit 0
fi

