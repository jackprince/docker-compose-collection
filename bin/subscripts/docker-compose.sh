#!/usr/bin/env bash

DOCKER_COMPOSE_FILE_PARAM="-f docker-compose.yml"
if [[ $(grep -r LINK_PORTS .env | cut -d= -f2) == "true" ]]; then
    DOCKER_COMPOSE_FILE_PARAM="${DOCKER_COMPOSE_FILE_PARAM} -f docker-compose-ports.yml"
fi

if [[ -f "docker-compose-override.yml" ]]; then
    DOCKER_COMPOSE_FILE_PARAM="${DOCKER_COMPOSE_FILE_PARAM} -f docker-compose-override.yml"
fi

echo ${DOCKER_COMPOSE_FILE_PARAM}
docker-compose ${DOCKER_COMPOSE_FILE_PARAM} ${@}