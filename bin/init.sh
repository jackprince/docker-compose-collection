#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR=$(realpath "$(dirname ${0})/../")

if [[ ! -z ${1} ]] && [[ -f ${1} ]]; then
    SOURCE_ENV=$(realpath ${1})
else
    SOURCE_ENV=${DIR}/.env.example
fi

DESTINATION_ENV=${DIR}/.env

printf "Copy \"${SOURCE_ENV}\" to \"${DESTINATION_ENV}\" \n"
cp ${SOURCE_ENV} ${DESTINATION_ENV}

cd ${DIR}

NEW_UID=$(id -u)
printf "Set param \"UID\" to \"${NEW_UID}\" \n"
sed -i "s|^\(UID=\s*\).*$|\1${NEW_UID}|" .env

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
