#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

check() {
    SERVICES=${1}
    CONFIG_KEY=${2}
    SERVICE_NAME=${3}

    if [[ ! $(grep -r ${CONFIG_KEY} .env | cut -d= -f2) == "true" ]]; then
        SERVICES=$(echo -n ${SERVICES} | sed "s|${SERVICE_NAME}||")
    fi

    echo -n ${SERVICES}
}

if [[ -z ${1} ]]; then
    SERVICES=$(docker-compose ps --services | paste -sd " ")

    SERVICES=$(check "${SERVICES}" "DEFAULT_START_PHPMYADMIN" "phpmyadmin")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_MAILHOG" "mailhog")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_CHROME" "selenium-chrome")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_SELENIUM_FIREFOX" "selenium-firefox")
    SERVICES=$(check "${SERVICES}" "DEFAULT_START_HTTPBIN" "httpbin")
else
    SERVICES=${@}
fi

echo "Starting the following services: ${SERVICES}"

./bin/subscripts/docker-compose.sh up -d ${SERVICES}
cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
