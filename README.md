# docker-compose-collection

Inspired by laradock this project contains a useful docker-compose.yml file for working with php, codeception, laravel, .... I preferred creating a collection by myself because there were several things i couldn't do with laradock.

# Usage
To use this project you just have to do 3 steps:
1. clone it
2. copy .env.example to .env
3. run ./bin/start.sh

# Docs
You can found detailed information about all included containers, configuration and customization in the [wiki](https://gitlab.com/Bosi/docker-compose-collection/wikis/home).
