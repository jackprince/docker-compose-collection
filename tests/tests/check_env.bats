#!/usr/bin/env bats

load helpers

run_script () {
    run bin/subscripts/check_env.sh
}

@test $(get_description "check all is ok when .env is correct") {
    run_script
    [ $status -eq 0 ]
}

@test $(get_description "check all is ok when .env contains more keys than .env.example") {
    printf "\nMY_NEW_KEY=value123\n" >> .env
    run_script
    [ $status -eq 0 ]
}

@test $(get_description "check script is failing when missing param in .env") {
    printf "\nMY_NEW_KEY=value123\n" >> .env.example
    run_script
    [ $status -eq 1 ]
}

@test $(get_description "check script is failing when timezone do not exist") {
    sed -i "s|^\(TIMEZONE=\s*\).*$|\1Not/Exist|" .env
    run_script
    [ $status -eq 1 ]
}

@test $(get_description "check script is failing when UID id 0") {
    sed -i "s|^\(UID=\s*\).*$|\10|" .env
    run_script
    [ $status -eq 1 ]
}
