#!/usr/bin/env bats

load helpers

run_script () {
    run bin/bash.sh -c date
}

run_cli_script () {
    run bin/dcc bash -c date
}

@test $(get_description "check bash is working") {
    run_script
    [ $status -eq 0 ]
}

@test $(get_description "check bash is working via cli") {
    run_cli_script
    [ $status -eq 0 ]
}

@test $(get_description "check if script fails when .env is missing") {
    check_env_test
}

