#!/usr/bin/env bash

setup () {
    cp .env.example .env
    cp .env.example .env.org
    mkdir -p app_dir
    sed -i "s|^\(APP_DIR=\s*\).*$|\1./app_dir|" .env
    clear_composer
    rm -f docker-compose-override.yml
}

teardown() {
    cp .env.org .env
    cp .env.org .env.example
    docker-compose down
    clear_composer
    rm .env .env.org
    rm -rf app_dir
    rm -f docker-compose-override.yml
}

clear_composer() {
    APP_DIR=$(grep -r APP_DIR .env | cut -d= -f2)
    if [[ -d ${APP_DIR} ]]; then
        cd ${APP_DIR}
        rm -rf composer.json composer.lock vendor/
        cd -
    fi
}

get_description() {
    echo -e "\e[1m$(basename ${BATS_TEST_FILENAME}):\e[0m ${1}"
}

check_env_test() {
    rm .env
    run_script
    [ $status -eq 1 ]
}

check_runtime() {
    [ $(printf '%s\n' "${lines[@]}" | grep INFO | grep runtime | grep "${1}" | wc -l) -eq 1 ]
}