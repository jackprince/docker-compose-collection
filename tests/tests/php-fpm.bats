#!/usr/bin/env bats

load helpers

@test $(get_description "check if composer cache is working") {
    ./bin/start.sh php-fpm
    ./bin/exec.sh rm -rf ~/.composer
    ./bin/exec.sh composer init -n
    run ./bin/exec.sh composer require rivsen/hello-world
    [ $(echo $output | grep -i "loading from cache" | wc -l) -eq 0 ]
    ./bin/exec.sh rm -r vendor/
    run ./bin/exec.sh composer install
    [ $(echo $output | grep -i "loading from cache" | wc -l) -eq 1 ]
}

@test $(get_description "check if cron is running") {
    ./bin/start.sh php-fpm
    run ./bin/exec.sh service cron status
    [ $(echo $output | grep -i "ok" | grep -i "running" | wc -l) -eq 1 ]
}

@test $(get_description "check if cron-scripts are copied") {
    echo "hello world 12345" > ./contexts/php-fpm/system-cron/test_cron
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" cat /etc/cron.d/test_cron
    [ $status -eq 0 ]
    [ $(echo $output | grep -i "hello world 12345" | wc -l) -eq 1 ]
}

@test $(get_description "check if memory limit is set correctly") {
    sed -i "s|^\(PHP_MEMORY_LIMIT=\s*\).*$|\1167|" .env
    sed -i "s|^\(PHP_UPLOAD_MAX_FILESIZE=\s*\).*$|\1168|" .env
    ./bin/build.sh php-fpm
    ./bin/start.sh php-fpm
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('memory_limit');"
    [ $(echo $output | grep -i "167M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('upload_max_filesize');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -r "echo ini_get('post_max_size');"
    [ $(echo $output | grep -i "168M" | wc -l) -eq 1 ]
}

@test $(get_description "check xdebug extension") {
    COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
    ./bin/start.sh php-fpm
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -v
    [ $(echo $output | grep -i "xdebug" | wc -l) -eq 0 ]
    sed -i "s|^\(PHP_EXT_XDEBUG=\s*\).*$|\1true|" .env
    ./bin/build.sh php-fpm
    ./bin/restart.sh php-fpm
    run docker exec -t "${COMPOSE_PROJECT_NAME}_php-fpm_1" php -v
    [ $(echo $output | grep -i "xdebug" | wc -l) -eq 1 ]
}